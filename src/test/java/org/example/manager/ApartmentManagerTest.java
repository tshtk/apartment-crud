package org.example.manager;

import org.example.data.Apartment;
import org.example.search.SearchRequest;
import org.example.exception.ItemNotFoundException;
import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ApartmentManagerTest {
    static ApartmentManager manager = new ApartmentManager();
    static Apartment threeRoomsApartment = new Apartment(1, "three rooms", 6_000_000, 84_5, true, true, 10, 10);
    static Apartment oneRoomsApartment1 = new Apartment(2, "one room", 2_600_000, 38_5, true, true, 3, 9);
    static Apartment twoRoomsApartment = new Apartment(3, "two rooms", 4_600_000, 65_0, true, false, 5, 9);
    static Apartment oneRoomsApartment2 = new Apartment(4, "one room", 2_590_000, 38_5, true, true, 3, 9);
    static Apartment oneRoomsApartment3 = new Apartment(5, "one room", 2_600_050, 38_5, true, true, 3, 9);
    static Apartment oneRoomsApartment4 = new Apartment(6, "one room", 2_600_000, 38_5, false, true, 3, 9);
    static Apartment oneRoomsApartment5 = new Apartment(7, "one room", 2_600_000, 38_5, true, false, 3, 9);
    static Apartment oneRoomsApartment6 = new Apartment(8, "one room", 2_600_000, 38_5, true, true, 2, 9);
    static Apartment oneRoomsApartment7 = new Apartment(9, "one room", 2_600_000, 38_5, true, true, 4, 9);
    static Apartment oneRoomsApartment8 = new Apartment(10, "one room", 2_600_000, 38_5, true, true, 3, 10);
    static Apartment oneRoomsApartment9 = new Apartment(11, "one room", 2_600_000, 38_5, true, true, 3, 8);
    static Apartment oneRoomsApartment10 = new Apartment(12, "one room", 2_600_000, 39_5, true, true, 3, 9);
    static Apartment oneRoomsApartment11 = new Apartment(13, "one room", 2_600_000, 38_0, true, true, 3, 9);

    static SearchRequest emptySearchRequest = new SearchRequest();
    static SearchRequest fullSearchRequest = new SearchRequest(new String[]{"one room"}, 2_600_000, 2_600_000, 38_5, 38_5, true, true, 3, 3, 9, 9);

    @Test
    @Order(1)
    void shouldCreate() {
        List<Apartment> expectedAll = new ArrayList<>(0);
        List<Apartment> actualAll = manager.getAll();
        assertArrayEquals(expectedAll.toArray(), actualAll.toArray());

        manager.create(threeRoomsApartment);
        manager.create(oneRoomsApartment1);
        manager.create(twoRoomsApartment);
        manager.create(oneRoomsApartment2);
        manager.create(oneRoomsApartment3);
        manager.create(oneRoomsApartment4);
        manager.create(oneRoomsApartment5);
        manager.create(oneRoomsApartment6);
        manager.create(oneRoomsApartment7);
        manager.create(oneRoomsApartment8);
        manager.create(oneRoomsApartment9);
        manager.create(oneRoomsApartment10);
        manager.create(oneRoomsApartment11);
        Collections.addAll(expectedAll, threeRoomsApartment, oneRoomsApartment1, twoRoomsApartment);
        Collections.addAll(expectedAll, oneRoomsApartment2, oneRoomsApartment3, oneRoomsApartment4, oneRoomsApartment5);
        Collections.addAll(expectedAll, oneRoomsApartment6, oneRoomsApartment7, oneRoomsApartment8, oneRoomsApartment9);
        Collections.addAll(expectedAll, oneRoomsApartment10, oneRoomsApartment11);
        actualAll = manager.getAll();
        assertArrayEquals(expectedAll.toArray(), actualAll.toArray());
    }

    @Order(2)
    @Test
    void shouldNotReturnById() {
        long id = 999;
        Assertions.assertThrows(ItemNotFoundException.class, () -> {
            manager.getById(id);
        });
    }

    @Order(3)
    @Test
    void shouldReturnAllWithLimit() {
        List<Apartment> expectedAll = new ArrayList<>();
        expectedAll.add(threeRoomsApartment);
        manager.getAll();
        List<Apartment> actualAll = manager.getAll(1);
        assertArrayEquals(expectedAll.toArray(), actualAll.toArray());
    }

    @Order(4)
    @Test
    void shouldUpdate() {
        long id = 3;
        int expected = 4_650_000;
        Apartment tmp = new Apartment(manager.getById(id));
        tmp.setPrice(expected);
        manager.update(tmp);
        int actual = manager.getById(id).getPrice();
        assertEquals(expected, actual);
    }
    @Order(5)
    @Test
    void shouldNotUpdate() {
        Assertions.assertThrows(ItemNotFoundException.class, () -> {
            manager.update(new Apartment());
        });
    }
    @Test
    @Order(6)
    void shouldSearchAllResults() {
        List<Apartment> actual = manager.searchBy(emptySearchRequest);
        List<Apartment> expected = manager.getAll();
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    @Order(7)
    void shouldSearchOneResults() {
        List<Apartment> searchResult = manager.searchBy(fullSearchRequest);
        int actualSize = searchResult.size();
        int expectedSize = 1;
        System.out.println(searchResult);
        assertEquals(expectedSize, actualSize);
        List<Apartment> expected = new ArrayList<>();
        expected.add(oneRoomsApartment1);
        System.out.println(expected);
        List<Apartment> actual = searchResult;
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    @Order(8)
    void shouldRemoveNotExistent() {
        int expectedCount = manager.getCount();

        Assertions.assertThrows(ItemNotFoundException.class, () -> {
            manager.removeById(999);
        });

        int actualCount = manager.getCount();
        assertEquals(expectedCount, actualCount);
    }

    @Test
    @Order(9)
    void shouldRemoveExistent() {
        int expectedCount = manager.getCount() - 1;
        manager.removeById(oneRoomsApartment1.getId());
        int actualCount = manager.getCount();
        assertEquals(expectedCount, actualCount);
    }


}
