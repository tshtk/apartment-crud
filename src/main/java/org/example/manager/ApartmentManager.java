package org.example.manager;


import lombok.extern.slf4j.Slf4j;
import org.example.data.Apartment;
import org.example.search.SearchRequest;
import org.example.exception.ItemNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
public class ApartmentManager {
    private long nextId = 1;
    private final List<Apartment> items = new ArrayList<>();

    public Apartment create(final Apartment item) {
        item.setId(nextId);
        log.debug("create item, nextId: {}, apartment: {}", nextId, item);
        items.add(item);
        nextId += 1;
        return item;
    }

    public Apartment getById(final long id) {
        for (final Apartment item : items) {
            if (item.getId() == id) {
                log.debug("return item: {}", item);
                return item;
            }
        }
        log.debug("item by id: {} not found", id);
        throw new ItemNotFoundException(id);
    }

    public List<Apartment> getAll() {
        final List<Apartment> results = new ArrayList<>(items);
        log.debug("return list of apartments, size: {}", results.size());
        return results;
    }

    public List<Apartment> getAll(final int limit) {
        final List<Apartment> results = new ArrayList<>(items.subList(0, limit));
        log.debug("return list (with limit {}) of apartments, size: {} ", limit, results.size());
        return results;
    }

    public void removeById(final long id) {
        final boolean removed = items.removeIf(o -> o.getId() == id);
        if (!removed) {
            log.debug("remove item by Id: {} fault", id);
            throw new ItemNotFoundException(id);
        }
        log.debug("remove item by id: {} success", id);
    }

    public Apartment update(final Apartment item) {
        final int index = getIndexById(item.getId());
        if (index == -1) {
            throw new ItemNotFoundException(item.getId());
        }
        log.debug("update item: {}", item);
        items.set(index, item);
        return item;
    }

    public List<Apartment> searchBy(final SearchRequest request) {
        final List<Apartment> results = new ArrayList<>();
        for (Apartment item : items) {

            if (request.getNumberOfRooms() != null && !isNumberOfRoomsMatched(request, item)) {
                continue;
            }
            if (request.getPriceMax() != null && item.getPrice() > request.getPriceMax()) {
                continue;
            }
            if (request.getPriceMin() != null && item.getPrice() < request.getPriceMin()) {
                continue;
            }
            if (request.getAreaMax() != null && item.getArea() > request.getAreaMax()) {
                continue;
            }
            if (request.getAreaMin() != null && item.getArea() < request.getAreaMin()) {
                continue;
            }
            if (request.isBalcony() && !item.isBalcony()) {
                continue;
            }
            if (request.isLoggia() && !item.isLoggia()) {
                continue;
            }
            if (request.getFloorMax() != null && item.getFloor() > request.getFloorMax()) {
                continue;
            }
            if (request.getFloorMin() != null && item.getFloor() < request.getFloorMin()) {
                continue;
            }
            if (request.getFloorsInHouseMax() != null && item.getFloorsInHouse() > request.getFloorsInHouseMax()) {
                continue;
            }
            if (request.getFloorsInHouseMin() != null && item.getFloorsInHouse() < request.getFloorsInHouseMin()) {
                continue;
            }
            results.add(item);
            }
        log.debug("search completed, found: {} items", results.size());
        return results;
    }

    private boolean isNumberOfRoomsMatched(SearchRequest request, Apartment apartment) {
        boolean numberOfRoomsMatched = false;
        for (String numbersOfRooms : request.getNumberOfRooms()) {
            if (Objects.equals(numbersOfRooms, apartment.getNumberOfRooms())) {
                numberOfRoomsMatched = true;
                break;
            }
        }
        return numberOfRoomsMatched;
    }

    public int getCount() {
        return items.size();
    }

    private int getIndexById(final long id) {
        for (int i = 0; i < items.size(); i++) {
            final Apartment apartment = items.get(i);
            if (apartment.getId() == id) {
                log.debug("return index: {} by id: {}", i, id);
                return i;
            }
        }
        log.debug("index by id: {} not found", id);
        return -1;
    }
}
